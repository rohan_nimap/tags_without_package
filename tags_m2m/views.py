from django.shortcuts import render
from .models import Tags,Candidate,Tagged
from rest_framework import generics
from tags_m2m.serializers import TaggedSerializer,TagSerializer,CandidateSerializer


class CandidateList(generics.ListCreateAPIView):
    # List all the candidates (GET) 
    # Endpoint :- http://127.0.0.1:8000/api/v1/candidates/
    # Create candidates (POST) 
    # Endpoint :- http://127.0.0.1:8000/api/v1/candidates/
    serializer_class = CandidateSerializer
    queryset = Candidate.objects.all()

    def get_serializer_class(self):
        switcher = {
            'GET' : CandidateSerializer,
            'POST' : CandidateSerializer,
        }
        return switcher.get(self.request.method)

class TagsList(generics.ListCreateAPIView):
    # LIst existing tags
    # Endpoint :- http://127.0.0.1:8000/api/v1/list/tags/
    serializer_class = TagSerializer
    queryset = Tags.objects.all()

class DetailedCandidateList(generics.RetrieveUpdateDestroyAPIView):
    # create tags
    # ENdpoint :- http://127.0.0.1:8000/api/v1/list/candidates/pk/
    serializer_class = CandidateSerializer
    queryset = Candidate.objects.all()

class Tagthem(generics.CreateAPIView):
    # Endpoint for through table.
    # http://127.0.0.1:8000/api/v1/tagthem/
    serializer_class = TaggedSerializer
    queryset = Candidate.objects.all()