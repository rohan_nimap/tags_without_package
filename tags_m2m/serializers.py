from rest_framework import serializers
from . import models

class TagSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['id','title']
    #    fields = '__all__'
        model = models.Tags

class CandidateSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Candidate
        depth = 1 # TO show values of the foreign key and not the index, creates nested json output.

class TaggedSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Tagged
