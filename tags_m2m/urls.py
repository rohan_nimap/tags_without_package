from django.contrib import admin
from django.urls import path,include
from . import views
from .views import CandidateList,TagsList

urlpatterns = [
    # All views based on Candidate
    path('candidates/',views.CandidateList.as_view()),
    path('tags/',views.TagsList.as_view()),
    path('candidates/<int:pk>',views.DetailedCandidateList.as_view()),
    path('tagthem/',views.Tagthem.as_view()),
]
