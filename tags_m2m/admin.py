from django.contrib import admin
#from .models import Book,Author,Authored
from .models import Tags,Candidate,Tagged
# Register your models here.

admin.site.register(Tags)
admin.site.register(Candidate)
admin.site.register(Tagged)