from django.db import models

# Create your models here.


class Tags(models.Model):
    title = models.CharField(max_length=200) # the tag name, make unique=True to not allow duplicate values.
    candidate = models.ManyToManyField('Candidate',through="Tagged",related_name="tags_candidate") # candidate Foreign Key

    def __str__(self):
        return self.title

    class Meta:
        db_table = "tags"

class Candidate(models.Model):
    name = models.CharField(max_length=200) # Name of the candidate.
    tags = models.ManyToManyField('Tags',through='Tagged',related_name="candidate_tags") # tags Foreign Key

    def __str__(self):
        return self.name

    class Meta:
        db_table = "candidate"

class Tagged(models.Model):
    # the through table which joins the foreign keys of other two tables.
    tags = models.ForeignKey(Candidate,on_delete=models.CASCADE,null=True)
    Candidates = models.ForeignKey(Tags,on_delete=models.CASCADE,null=True)
