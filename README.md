## TAGS Implementation without package

Basic tags, using ManyToManyField 

Django Version :- 2.2.4

## Step 1:- 
Populate the Candidate table using the endpoint
### (Candidate Create) EndPoint :-
`http://127.0.0.1:8000/api/v1/candidates/`

## Step 2 :-
Populate the tags tables using the endpoint
### (Tags Create) Endpoint :-
`http://127.0.0.1:8000/api/v1/tags/`

## Step 3 :-
Add the tag to the particular candidate using the endpoint
### (Add tags to Candidate) Endpoint :-
`http://127.0.0.1:8000/api/v1/tagthem/`

## Step 4 :-
List All tags ( unique=True in Models ensures that there are no duplicate tags ),
Output of this could be used to show suggestions to the users.
### (List tags) Endpoint :-
`http://127.0.0.1:8000/api/v1/tags/`

## Step 5 :-
List All Candidates 

`http://127.0.0.1:8000/api/v1/candidates/`
or 
`http://127.0.0.1:8000/api/v1/candidates/pk`
Sample output :-
```
{
    "id": 2,
    "name": "rohan",
    "tags": [
        {
            "id": 3,
            "title": "c++",
            "candidate": [
                2,
                6
            ]
        },
        {
            "id": 2,
            "title": "java",
            "candidate": [
                2,
                6
            ]
        },
        {
            "id": 1,
            "title": "python",
            "candidate": [
                2,
                6
            ]
        }
    ]
}
```

* candidate in tags shows which other candidate has that tag enabled, so it could be easily filtered.

### Thanks !!!

